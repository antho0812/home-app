package com.example.myhomeapp.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;


public class AppUtils {

    public static void startActivity(Activity context, Class clazz){
        Intent salida = new Intent(context, clazz);
        context.startActivity(salida);
    }

    public static void startActivityForResult(Activity context, Class clazz, int requestCode){
        Intent salida = new Intent(context, clazz);
        context.startActivityForResult(salida, requestCode);
    }

    public static void showMessageDialog(Activity context ,String titulo, String mensaje){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensaje);
        builder.create();
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showMessageDialog(Activity context ,String titulo, String mensaje,DialogInterface.OnClickListener action ){
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensaje);
        builder.create();
        builder.setPositiveButton("Aceptar",action);
        builder.show();
    }

    public static void initProgress(ProgressBar progressBar){
        progressBar.setVisibility(View.VISIBLE);
    }

    public static void stopProgress(ProgressBar progressBar){
        progressBar.setVisibility(View.GONE);
    }

}
