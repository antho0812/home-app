package com.example.myhomeapp.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.myhomeapp.R;
import com.example.myhomeapp.utils.AppUtils;

public class SignUpActivity extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        progressBar = this.findViewById(R.id.app_progress_bar);
    }

    public void finishThisActivity(View view){
        finish();
    }

    public void openRegistroActivity(View view){
        AppUtils.startActivity(this, RegistroActivity.class);
    }

    public void googleSingUp(View view){
        AppUtils.initProgress(progressBar);
    }

    public void facebookSingUp(View view){
        AppUtils.initProgress(progressBar);
    }
}
