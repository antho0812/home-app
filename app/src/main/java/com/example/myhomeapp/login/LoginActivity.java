package com.example.myhomeapp.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.myhomeapp.R;
import com.example.myhomeapp.utils.AppUtils;

public class LoginActivity extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressBar = this.findViewById(R.id.app_progress_bar);
    }

    public void openSignUpActivity(View view){
        AppUtils.startActivity(this, SignUpActivity.class);
    }

    public void iniciarSesion(View view){
        AppUtils.initProgress(progressBar);
        AppUtils.showMessageDialog(this, "Credenciales Incorrectas", "Usuario o contraseña incorrectos", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AppUtils.stopProgress(progressBar);
            }
        });
    }
}
