package com.example.myhomeapp.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.myhomeapp.DrawerAct;
import com.example.myhomeapp.R;
import com.example.myhomeapp.utils.AppUtils;

public class RegistroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
    }

    public void finishThisActivity(View view){
        this.finish();
    }

    public void startHomeActivity(View view){
        AppUtils.startActivity(this, DrawerAct.class);
    }
}
